import requests
import urllib
import datetime
import time
from enum import Enum
import hashlib

# https://www.boerse-frankfurt.de/indices/dax

host = "https://api.boerse-frankfurt.de"
api_version = 1

history_path = "tradingview/lightweight/history/single"

class Indication(Enum):
    Xetra = "XETR"
    Ariva = "ARIVA"
    Xfra = "XFRA"


class Resolution(Enum):
    minute = "M"
    hour = "H"
    day = "D"
    month = "MO"


def construct_url(path: str) -> str:
    url = urllib.parse.urljoin(host, f"v{api_version}/")
    return urllib.parse.urljoin(url, path)

def search(search_terms: str) -> []:
    """
    https://api.boerse-frankfurt.de/v1/global_search/limitedsearch/en?searchTerms=IE00B4L5Y983
    Returns all stocks found for the search_terms
    :param search_terms:
    :return: list of all found stocks
    """
    url = construct_url(f"global_search/limitedsearch/en?searchTerms={search_terms}")
    r = requests.get(url)
    if not r.ok:
        return []

    return r.json()

def generate_header(url: str) -> {}:
    # source: https://lwthiker.com/reversing/2022/02/12/analyzing-stock-exchange-api.html
    dt = datetime.datetime.now()
    dt_str = dt.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

    # main-es ... :
    #  generateHeaders(t){
    #   const e=i().toISOString();
    #   let n=e+t+r.N.tracing.salt;
    #   return n=s.V.hashStr(n).toString(),{"Client-Date":e,"X-Client-TraceId":n,"X-Security":s.V.hashStr(i().format("YYYYMMDDHHmm")).toString()}}}}
    # definition of t: setHeaders:un.N.generateHeaders(t.urlWithParams)}
    salt = "w4ivc1ATTGta6njAZzMbkL3kJwxMfEAKDa3MNr"  # found in main-es....js
    trace_id = dt_str + url + salt

    # finding out hash function
    # main-es.js: "5d41402abc4b2a76b9719d911017c592"!==n.hashStr("hello")&&console.error("Md5 self test failed.")
    # --> md5 hashing
    trace_id_hashed = hashlib.md5(trace_id.encode()).hexdigest()

    # not mandatory
    security = hashlib.md5(dt.strftime("YYYYMMDDHHmm").encode()).hexdigest()

    headers = {'Client-Date': dt_str,
               'X-Client-TraceId': trace_id_hashed,
               'X-Security': security
               }
    return headers
def instrument_information_slug(slug: str, instrument_type: str) -> {}:
    """
    "https://api.boerse-frankfurt.de/v1/data/instrument_information?slug=ishares-core-msci-world-ucits-etf&instrumentType=ETP"
    :param slug: can be the slug, isin
    :param instrument_type:
    :return: {}
    example return:
        {
            "isin":"IE00B4L5Y983",
             "wkn":"A0RPWH",
             "slug":"ishares-core-msci-world-ucits-etf",
             "defaultMic":"XETR",
             "exchangeSymbol":"EUNL",
             "indexConstituentsMic":null,
             "instrumentName":{
                "originalValue":"iShares Core MSCI World UCITS ETF",
                "translations":{}
             },
             "instrumentTypeKey":"etf",
             "market":"REGULATED",
             "mics":["XETR","XFRA"],
             "segment":"STANDARD",
             "hasSustainabilityData":false,
             "hasRelatedData":false
         }
    """

    url = construct_url(f"data/instrument_information?slug={slug}&instrumentType={instrument_type}")

    headers = generate_header(url)


    r = requests.get(url, headers=headers)
    if not r.ok:
        return {}

    res = r.json()
    return res

def instrument_information_isin(isin: str, instrument_type: str) -> {}:
    """
    "https://api.boerse-frankfurt.de/v1/data/instrument_information?slug=ishares-core-msci-world-ucits-etf&instrumentType=ETP"
    :param isin: isin number
    :param instrument_type:
    :return: {}
    example return:
        {
            "isin":"IE00B4L5Y983",
             "wkn":"A0RPWH",
             "slug":"ishares-core-msci-world-ucits-etf",
             "defaultMic":"XETR",
             "exchangeSymbol":"EUNL",
             "indexConstituentsMic":null,
             "instrumentName":{
                "originalValue":"iShares Core MSCI World UCITS ETF",
                "translations":{}
             },
             "instrumentTypeKey":"etf",
             "market":"REGULATED",
             "mics":["XETR","XFRA"],
             "segment":"STANDARD",
             "hasSustainabilityData":false,
             "hasRelatedData":false
         }
    """

    url = construct_url(f"data/instrument_information?isin={isin}&instrumentType={instrument_type}")

    headers = generate_header(url)


    r = requests.get(url, headers=headers)
    if not r.ok:
        return {}

    res = r.json()
    return res

# does not work!
# def instrument_information_wkn(wkn: str, instrument_type: str) -> {}:
#     """
#     "https://api.boerse-frankfurt.de/v1/data/instrument_information?slug=ishares-core-msci-world-ucits-etf&instrumentType=ETP"
#     :param wkn: wkn number
#     :param instrument_type:
#     :return: {}
#     example return:
#         {
#             "isin":"IE00B4L5Y983",
#              "wkn":"A0RPWH",
#              "slug":"ishares-core-msci-world-ucits-etf",
#              "defaultMic":"XETR",
#              "exchangeSymbol":"EUNL",
#              "indexConstituentsMic":null,
#              "instrumentName":{
#                 "originalValue":"iShares Core MSCI World UCITS ETF",
#                 "translations":{}
#              },
#              "instrumentTypeKey":"etf",
#              "market":"REGULATED",
#              "mics":["XETR","XFRA"],
#              "segment":"STANDARD",
#              "hasSustainabilityData":false,
#              "hasRelatedData":false
#          }
#     """
#
#     url = construct_url(f"data/instrument_information?wkn={wkn}&instrumentType={instrument_type}")
#
#     headers = generate_header(url)
#
#
#     r = requests.get(url, headers=headers)
#     if not r.ok:
#         return {}
#
#     res = r.json()
#     return res

def history_raw(startDate: datetime.datetime,
            endDate: datetime.datetime,
            isin: str,
            indicator: Indication = Indication.Xetra,
            resolution: Resolution = Resolution.day,
            isKeepResolutionForLatestWeeksIfPossible: bool = False):
    is_ask_price = False
    sd = int(time.mktime(startDate.timetuple()))
    ed = int(time.mktime(endDate.timetuple()))
    url_path = urllib.parse.urljoin(history_path, f"?resolution={resolution.value}&isKeepResolutionForLatestWeeksIfPossible={isKeepResolutionForLatestWeeksIfPossible}&from={sd}&to={ed}&isBidAskPrice={is_ask_price}&symbols={indicator.value}:{isin}")
    url = construct_url(url_path)

    r = requests.get(url)

    if not r.ok:
        return []

    return r.json()

def history(startDate: datetime.datetime,
            endDate: datetime.datetime,
            isin: str,
            indicator: Indication = Indication.Xetra,
            resolution: Resolution = Resolution.day,
            isKeepResolutionForLatestWeeksIfPossible: bool = False) -> []:
    """
    returns a tuple of time value list. The time is the unix time in milliseconds
    """
    res = history_raw(startDate, endDate, isin, indicator, resolution, isKeepResolutionForLatestWeeksIfPossible)
    if len(res) == 0 or type(res[0]) is not dict:
        return [], []

    if "quotes" not in res[0]:
        return [], []

    # to keep it simple do not use numpy
    time = []
    value = []
    count = res[0]["quotes"]["count"]
    tv = res[0]["quotes"]["timeValuePairs"]

    for v in tv:
        time.append(v["time"] * 1000)
        value.append(v["value"])

    return time, value

# price history
# https://api.boerse-frankfurt.de/v1/data/price_history?limit=50&offset=0&isin=DE0005933931&mic=XETR&minDate=2021-11-04&maxDate=2022-11-04&cleanSplit=false&cleanPayout=false&cleanSubscriptionRights=false

def priceHistory(isin:str, market: Indication, startDate: datetime.datetime, endDate: datetime.datetime, limit: int = 50, offset: int = 0, cleanSplit: bool = False, cleanPayout: bool = False, cleanSubscriptionRights: bool = False):
    """
    Example
    {'isin': 'IE00B4L5Y983', 'data': [{'date': '2022-11-04', 'open': 71.124, 'close': 70.528, 'high': 71.634, 'low': 70.326, 'turnoverPieces': 584801, 'turnoverEuro': 41668320.95}, {'date': '2022-11-03', 'open': 71.332, 'close': 71.182, 'high': 71.474, 'low': 70.526, 'turnoverPieces': 374779, 'turnoverEuro': 26633886.46}, {'date': '2022-11-02', 'open': 72.542, 'close': 71.99, 'high': 72.786, 'low': 71.934, 'turnoverPieces': 238217, 'turnoverEuro': 17204550.91}, {'date': '2022-11-01', 'open': 72.702, 'close': 72.418, 'high': 73.002, 'low': 72.168, 'turnoverPieces': 771197, 'turnoverEuro': 56070587.3}, {'date': '2022-10-31', 'open': 72.26, 'close': 72.402, 'high': 72.71, 'low': 72.01, 'turnoverPieces': 462312, 'turnoverEuro': 33426706}, {'date': '2022-10-28', 'open': 70.382, 'close': 71.788, 'high': 71.92, 'low': 70.348, 'turnoverPieces': 497445, 'turnoverEuro': 35313543.75}, {'date': '2022-10-27', 'open': 70.834, 'close': 71.256, 'high': 71.51, 'low': 70.6, 'turnoverPieces': 359052, 'turnoverEuro': 25478596.25}, {'date': '2022-10-26', 'open': 70.99, 'close': 71.428, 'high': 71.476, 'low': 70.722, 'turnoverPieces': 716653, 'turnoverEuro': 50886272.44}, {'date': '2022-10-25', 'open': 71.088, 'close': 71.314, 'high': 71.314, 'low': 70.662, 'turnoverPieces': 398192, 'turnoverEuro': 28237016.06}, {'date': '2022-10-24', 'open': 70.59, 'close': 70.592, 'high': 71.132, 'low': 70.024, 'turnoverPieces': 341191, 'turnoverEuro': 24067910.63}, {'date': '2022-10-21', 'open': 69.332, 'close': 69.786, 'high': 70.02, 'low': 68.962, 'turnoverPieces': 423291, 'turnoverEuro': 29372095.24}, {'date': '2022-10-20', 'open': 69.746, 'close': 70.13, 'high': 70.38, 'low': 69.5, 'turnoverPieces': 359335, 'turnoverEuro': 25134676.66}, {'date': '2022-10-19', 'open': 70.246, 'close': 69.956, 'high': 70.518, 'low': 69.88, 'turnoverPieces': 1079031, 'turnoverEuro': 75689724.6}, {'date': '2022-10-18', 'open': 70.152, 'close': 69.716, 'high': 70.802, 'low': 69.716, 'turnoverPieces': 610573, 'turnoverEuro': 42999061.59}, {'date': '2022-10-17', 'open': 69.024, 'close': 69.592, 'high': 69.952, 'low': 68.718, 'turnoverPieces': 719984, 'turnoverEuro': 49794563.48}, {'date': '2022-10-14', 'open': 69.966, 'close': 68.892, 'high': 70.434, 'low': 68.85, 'turnoverPieces': 655869, 'turnoverEuro': 45678932.48}, {'date': '2022-10-13', 'open': 68.356, 'close': 68.766, 'high': 69.216, 'low': 67.144, 'turnoverPieces': 675732, 'turnoverEuro': 45800353.29}, {'date': '2022-10-12', 'open': 68.794, 'close': 68.634, 'high': 69.2, 'low': 68.384, 'turnoverPieces': 464523, 'turnoverEuro': 31960088.33}, {'date': '2022-10-11', 'open': 68.612, 'close': 68.88, 'high': 69.056, 'low': 68.3, 'turnoverPieces': 436712, 'turnoverEuro': 29988215.26}, {'date': '2022-10-10', 'open': 69.386, 'close': 69.28, 'high': 69.91, 'low': 69.096, 'turnoverPieces': 315063, 'turnoverEuro': 21889518.64}, {'date': '2022-10-07', 'open': 70.728, 'close': 69.68, 'high': 71.03, 'low': 69.668, 'turnoverPieces': 544348, 'turnoverEuro': 38251669.6}, {'date': '2022-10-06', 'open': 70.958, 'close': 71.084, 'high': 71.434, 'low': 70.48, 'turnoverPieces': 295758, 'turnoverEuro': 20968832.97}, {'date': '2022-10-05', 'open': 70.368, 'close': 70.55, 'high': 70.74, 'low': 70.25, 'turnoverPieces': 790401, 'turnoverEuro': 55673757.58}, {'date': '2022-10-04', 'open': 69.976, 'close': 70.676, 'high': 70.804, 'low': 69.97, 'turnoverPieces': 706445, 'turnoverEuro': 49738497.57}, {'date': '2022-10-03', 'open': 68.206, 'close': 69.162, 'high': 69.316, 'low': 67.75, 'turnoverPieces': 827440, 'turnoverEuro': 56530859.35}, {'date': '2022-09-30', 'open': 68.95, 'close': 69.26, 'high': 69.5, 'low': 68.62, 'turnoverPieces': 1121012, 'turnoverEuro': 77416531.71}, {'date': '2022-09-29', 'open': 70.208, 'close': 68.84, 'high': 70.208, 'low': 68.646, 'turnoverPieces': 264389, 'turnoverEuro': 18373886.26}, {'date': '2022-09-28', 'open': 69.984, 'close': 70.476, 'high': 70.624, 'low': 69.414, 'turnoverPieces': 445470, 'turnoverEuro': 31163645.53}, {'date': '2022-09-27', 'open': 70.578, 'close': 70.244, 'high': 70.964, 'low': 70.244, 'turnoverPieces': 1463605, 'turnoverEuro': 103424168.42}, {'date': '2022-09-26', 'open': 70.162, 'close': 70.384, 'high': 70.85, 'low': 69.812, 'turnoverPieces': 577442, 'turnoverEuro': 40570566.12}, {'date': '2022-09-23', 'open': 70.902, 'close': 70.126, 'high': 70.972, 'low': 70.06, 'turnoverPieces': 552820, 'turnoverEuro': 38946497.06}, {'date': '2022-09-22', 'open': 71.088, 'close': 70.938, 'high': 71.96, 'low': 70.854, 'turnoverPieces': 341014, 'turnoverEuro': 24294207.25}, {'date': '2022-09-21', 'open': 71.792, 'close': 72.528, 'high': 72.588, 'low': 71.75, 'turnoverPieces': 265093, 'turnoverEuro': 19132718.08}, {'date': '2022-09-20', 'open': 72.314, 'close': 71.596, 'high': 72.53, 'low': 71.444, 'turnoverPieces': 419006, 'turnoverEuro': 30152425.44}, {'date': '2022-09-19', 'open': 71.648, 'close': 71.67, 'high': 72.102, 'low': 71.25, 'turnoverPieces': 193079, 'turnoverEuro': 13820420.34}, {'date': '2022-09-16', 'open': 72.046, 'close': 71.52, 'high': 72.246, 'low': 71.45, 'turnoverPieces': 279972, 'turnoverEuro': 20139935.37}, {'date': '2022-09-15', 'open': 73.628, 'close': 72.864, 'high': 73.706, 'low': 72.614, 'turnoverPieces': 498196, 'turnoverEuro': 36548234.4}, {'date': '2022-09-14', 'open': 73.53, 'close': 73.318, 'high': 73.606, 'low': 72.938, 'turnoverPieces': 327632, 'turnoverEuro': 24038002.07}, {'date': '2022-09-13', 'open': 75.326, 'close': 73.862, 'high': 75.512, 'low': 73.752, 'turnoverPieces': 485020, 'turnoverEuro': 36370260.42}, {'date': '2022-09-12', 'open': 74.304, 'close': 75.18, 'high': 75.338, 'low': 74.15, 'turnoverPieces': 318446, 'turnoverEuro': 23808039.21}, {'date': '2022-09-09', 'open': 73.798, 'close': 74.672, 'high': 74.834, 'low': 73.742, 'turnoverPieces': 200026, 'turnoverEuro': 14831365.04}, {'date': '2022-09-08', 'open': 73.61, 'close': 74.12, 'high': 74.142, 'low': 72.93, 'turnoverPieces': 278108, 'turnoverEuro': 20426361.69}, {'date': '2022-09-07', 'open': 72.788, 'close': 73.07, 'high': 73.168, 'low': 72.686, 'turnoverPieces': 365821, 'turnoverEuro': 26675958.5}, {'date': '2022-09-06', 'open': 73.326, 'close': 73.318, 'high': 73.784, 'low': 72.752, 'turnoverPieces': 316822, 'turnoverEuro': 23249434.18}, {'date': '2022-09-05', 'open': 73.512, 'close': 73.464, 'high': 73.512, 'low': 73.038, 'turnoverPieces': 417826, 'turnoverEuro': 30624931.57}, {'date': '2022-09-02', 'open': 73.35, 'close': 73.912, 'high': 74.074, 'low': 73.098, 'turnoverPieces': 1326308, 'turnoverEuro': 97289832.73}, {'date': '2022-09-01', 'open': 72.84, 'close': 72.912, 'high': 73.018, 'low': 72.362, 'turnoverPieces': 847457, 'turnoverEuro': 61683839.99}, {'date': '2022-08-31', 'open': 74.252, 'close': 73.428, 'high': 74.328, 'low': 73.32, 'turnoverPieces': 293884, 'turnoverEuro': 21729460.32}, {'date': '2022-08-30', 'open': 75.04, 'close': 73.984, 'high': 75.372, 'low': 73.75, 'turnoverPieces': 220258, 'turnoverEuro': 16468540.7}, {'date': '2022-08-29', 'open': 75.134, 'close': 74.784, 'high': 75.188, 'low': 74.45, 'turnoverPieces': 421001, 'turnoverEuro': 31523778.28}], 'totalCount': 258, 'tradedInPercent': False}
    :param isin:
    :param market:
    :param startDate:
    :param endDate:
    :param limit:
    :param offset:
    :param cleanSplit:
    :param cleanPayout:
    :param cleanSubscriptionRights:
    :return:
    """
    sd = startDate.strftime("%Y-%m-%d")
    ed = endDate.strftime("%Y-%m-%d")
    url = construct_url(f"data/price_history?limit={limit}&offset={offset}&isin={isin}&mic={market.value}&minDate={sd}&maxDate={ed}&cleanSplit={cleanSplit}&cleanPayout={cleanPayout}&cleanSubscriptionRights={cleanSubscriptionRights}")

    headers = generate_header(url)

    r = requests.get(url, headers=headers)
    if not r.ok:
        return {}

    res = r.json()
    return res

if __name__ == "__main__":
    print(instrument_information_slug("ishares-core-msci-world-ucits-etf", "ETP"))
    print(instrument_information_isin("DE000A1EXV47", "ETP"))
    print(priceHistory("IE00B4L5Y983", Indication.Xetra, datetime.datetime.strptime("2021-11-04", "%Y-%m-%d"), datetime.datetime.strptime("2022-11-04", "%Y-%m-%d")))








