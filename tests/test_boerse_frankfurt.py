import unittest
import datetime

from .context import api

class Test(unittest.TestCase):

    def test_history_raw(self):
        startDate = datetime.datetime.fromtimestamp(-1488457774)  # default value for max
        endDate = datetime.datetime.fromtimestamp(1667302226)
        isin = "DE0008469008"
        data = api.history_raw(startDate, endDate, isin)
        self.assertEqual(len(data), 1)
        self.assertEqual(data[0]["isin"], "DE0008469008")

    def test_history(self):
        startDate = datetime.datetime.fromtimestamp(-1488457774)  # default value for max
        endDate = datetime.datetime.fromtimestamp(1667302226)
        isin = "DE0008469008"
        time, value = api.history(startDate, endDate, isin)
        self.assertTrue(len(time) > 0)
        self.assertEqual(len(time), len(value))

    def test_instrument_information(self):
        res = api.instrument_information("ishares-core-msci-world-ucits-etf", "ETP")
        self.assertTrue(len(res) > 0)

    def test_instrument_information_isin(self):
        res = api.instrument_information("IE00B4L5Y983", "ETP")
        self.assertTrue(len(res) > 0)

if __name__ == '__main__':
    unittest.main()
